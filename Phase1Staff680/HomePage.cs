﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Phase1Staff680
{
    public partial class aFacultyForm : MetroFramework.Forms.MetroForm
    {
        public aFacultyForm()
        {
            InitializeComponent();
        }
        DBModel sp = new DBModel();
        List<employeeDetails> empFilter = new List<employeeDetails>();
        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void HomePage_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'software_projectDataSet.EMPLOYEEDETAILS' table. You can move, or remove it, as needed.
            //this.eMPLOYEEDETAILSTableAdapter.Fill(this.software_projectDataSet.EMPLOYEEDETAILS);

            FilterComboBox.Items.Add("Department");
            FilterComboBox.Items.Add("Job Title");
            FilterComboBox.Items.Add("Tenuredate");
            //FilterComboBox.Items.Add("Employment_Startdate");
            //FilterComboBox.Items.Add("Salary");
            List<employeeDetails> emp = new List<employeeDetails>();
            emp = sp.employeeDetails.ToList();
            showGrid.DataSource = emp;
            searchCombo.Items.Add("Department");
            searchCombo.Items.Add("Job Title");


        }

        private void aHomePageForm_Load(object sender, EventArgs e)
        {

        }

        private void metroComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (FilterComboBox.SelectedItem.ToString() == "Employment_Startdate")
            //{
            //    FilterCriteriaComboBox.Visible = false;
            //    metroDateTime1.Visible = true;
            //}
            empFilter = null;
            if (FilterComboBox.SelectedItem.ToString() == "Tenuredate")
            {
                FilterCriteriaComboBox.Visible = false;
            }
            if (FilterComboBox.SelectedItem.ToString() == "Department")
            {
                List<string> dept = new List<string>();
                dept = sp.employeeDetails.Select(x => x.department).Distinct().OrderBy(i=>i).ToList();
                FilterCriteriaComboBox.DataSource = dept;
            }
            if (FilterComboBox.SelectedItem.ToString() == "Job Title")
            {
                List<string> job = new List<string>();
                job= sp.employeeDetails.Select(x => x.job_title).Distinct().OrderBy(i => i).ToList();
                FilterCriteriaComboBox.DataSource = job;
            }
        }

        private void aFilterTab_Click(object sender, EventArgs e)
        {
            
            
        }

        private void searchEmployee_Click(object sender, EventArgs e)
        {
            
            List<employeeDetails> emp = new List<employeeDetails>();
            emp = sp.employeeDetails.ToList();
            string searchParameter = FilterComboBox.SelectedItem.ToString();
            switch (searchParameter) {
                case "Department":
                    string searchCriteria = FilterCriteriaComboBox.SelectedItem.ToString();
                    List<employeeDetails> empResult = emp.Where(x => x.department == searchCriteria).ToList();
                    employeeResult.DataSource = empResult;
                    empFilter = empResult;
                    break;
                case "Job Title":
                    string searchCriteriaj = FilterCriteriaComboBox.SelectedItem.ToString();
                    List<employeeDetails> empResultj = emp.Where(x => x.job_title == searchCriteriaj).ToList();
                    employeeResult.DataSource = empResultj;
                    empFilter = empResultj;
                    break;
                case "Tenuredate":
                    List<employeeDetails> empResultt = emp.Where(x => x.tenuredate !=null).ToList();
                    employeeResult.DataSource = empResultt;
                    empFilter = empResultt;
                    break;
            }
            
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            DateTime datef =Convert.ToDateTime(DateFrom.Text);
            DateTime datet =Convert.ToDateTime(DateTo.Text);
            string salary = string.Empty;
            decimal salary1 ;
            decimal salary2 ;
            List<employeeDetails> empf = new List<employeeDetails>();
            List<employeeDetails> empr = new List<employeeDetails>();
            
            if (rb1.Checked)
            {
                salary = rb1.Text;
                salary1 = Convert.ToDecimal(salary.Substring(salary.IndexOf("-")+1));
                //int i = (salary.IndexOf("-") - 1);
                //salary2 = Convert.ToDecimal(salary.Substring(0, (salary.IndexOf("-") - 1)));
                empf = empFilter.Where(x => x.annual_salary <= salary1 && x.employment_startdate >= datef && x.employment_startdate <= datet).ToList();
            }
            else if (rb2.Checked)
            {
                salary = rb2.Text;
                salary1 = Convert.ToDecimal(salary.Substring(salary.IndexOf("-")+1));
                salary2 = Convert.ToDecimal(salary.Substring(0, (salary.IndexOf("-"))));
                empf = empFilter.Where(x => x.annual_salary >= salary2 && x.annual_salary <= salary1 && x.employment_startdate >= datef && x.employment_startdate <= datet).ToList();
            }
           else if (rb3.Checked)
            {
                salary = rb3.Text;
                salary1 = Convert.ToDecimal(salary.Substring(salary.IndexOf("-")+1));
                salary2 = Convert.ToDecimal(salary.Substring(0, (salary.IndexOf("-"))));
                empf = empFilter.Where(x => x.annual_salary >= salary2 && x.annual_salary <= salary1 && x.employment_startdate >= datef && x.employment_startdate <= datet).ToList();
            }
           else if (rb4.Checked)
            {
                salary = rb4.Text;
                salary1 =Convert.ToDecimal(salary.Substring(salary.IndexOf(" ")));
                empf = empFilter.Where(x => x.annual_salary >= salary1 && x.employment_startdate >= datef && x.employment_startdate <= datet).ToList();
            }
            
            else
            {
                empf = empFilter.Where(x => x.employment_startdate >= datef && x.employment_startdate <= datet).ToList();
            }
            employeeResult.DataSource = empf;
            empFilter = empf;
        }

        private void metroGrid1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void searchCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (searchCombo.SelectedItem.ToString() == "Department")
            {
                string[] dept = sp.employeeDetails.Select(x => x.department).ToArray();


                searchPrameter.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                searchPrameter.AutoCompleteSource = AutoCompleteSource.CustomSource;

                var autoComplete = new AutoCompleteStringCollection();
                autoComplete.AddRange(dept);
                searchPrameter.AutoCompleteCustomSource = autoComplete;

            }
            if (searchCombo.SelectedItem.ToString() == "Job Title")
            {
                string[] job = sp.employeeDetails.Select(x => x.job_title).ToArray();


                searchPrameter.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                searchPrameter.AutoCompleteSource = AutoCompleteSource.CustomSource;

                var autoComplete = new AutoCompleteStringCollection();
                autoComplete.AddRange(job);
                searchPrameter.AutoCompleteCustomSource = autoComplete;
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            List<employeeDetails> empdetails = new List<employeeDetails>();
            empdetails = sp.employeeDetails.Where(x => x.department.Contains(searchPrameter.Text)).ToList();
            searchGrid.DataSource = empdetails;
        }
    }
}
