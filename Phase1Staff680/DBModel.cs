﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Phase1Staff680
{
  public partial  class DBModel: DbContext
    {
        public DBModel()
            : base("name=Phase1Staff680.Properties.Settings.software_projectConnectionString")
        {
            base.Configuration.ProxyCreationEnabled = false;
        }
        public virtual DbSet<employeeDetails> employeeDetails { get; set; }
    }
}
