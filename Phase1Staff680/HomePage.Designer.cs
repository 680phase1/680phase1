﻿namespace Phase1Staff680
{
    partial class aFacultyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.metroTabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.aShowTab = new MetroFramework.Controls.MetroTabPage();
            this.showGrid = new MetroFramework.Controls.MetroGrid();
            this.aSearchTab = new MetroFramework.Controls.MetroTabPage();
            this.searchButton = new MetroFramework.Controls.MetroButton();
            this.searchGrid = new MetroFramework.Controls.MetroGrid();
            this.searchCombo = new MetroFramework.Controls.MetroComboBox();
            this.searchPrameter = new MetroFramework.Controls.MetroTextBox();
            this.aFilterTab = new MetroFramework.Controls.MetroTabPage();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.btnFilter = new MetroFramework.Controls.MetroButton();
            this.rb4 = new MetroFramework.Controls.MetroRadioButton();
            this.rb3 = new MetroFramework.Controls.MetroRadioButton();
            this.rb2 = new MetroFramework.Controls.MetroRadioButton();
            this.rb1 = new MetroFramework.Controls.MetroRadioButton();
            this.lblsalaryRange = new MetroFramework.Controls.MetroLabel();
            this.DateTo = new MetroFramework.Controls.MetroDateTime();
            this.lblDateTo = new MetroFramework.Controls.MetroLabel();
            this.lblDateFrom = new MetroFramework.Controls.MetroLabel();
            this.DateFrom = new MetroFramework.Controls.MetroDateTime();
            this.employeeResult = new MetroFramework.Controls.MetroGrid();
            this.searchEmployee = new MetroFramework.Controls.MetroButton();
            this.FilterCriteriaComboBox = new MetroFramework.Controls.MetroComboBox();
            this.FilterComboBox = new MetroFramework.Controls.MetroComboBox();
            this.aGraphTab = new MetroFramework.Controls.MetroTabPage();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.metroTabControl1.SuspendLayout();
            this.aShowTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.showGrid)).BeginInit();
            this.aSearchTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.searchGrid)).BeginInit();
            this.aFilterTab.SuspendLayout();
            this.metroPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.employeeResult)).BeginInit();
            this.aGraphTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // metroTabControl1
            // 
            this.metroTabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.metroTabControl1.Controls.Add(this.aShowTab);
            this.metroTabControl1.Controls.Add(this.aSearchTab);
            this.metroTabControl1.Controls.Add(this.aFilterTab);
            this.metroTabControl1.Controls.Add(this.aGraphTab);
            this.metroTabControl1.Location = new System.Drawing.Point(0, 59);
            this.metroTabControl1.Name = "metroTabControl1";
            this.metroTabControl1.SelectedIndex = 0;
            this.metroTabControl1.Size = new System.Drawing.Size(1086, 616);
            this.metroTabControl1.TabIndex = 0;
            this.metroTabControl1.UseSelectable = true;
            // 
            // aShowTab
            // 
            this.aShowTab.Controls.Add(this.showGrid);
            this.aShowTab.HorizontalScrollbarBarColor = true;
            this.aShowTab.HorizontalScrollbarHighlightOnWheel = false;
            this.aShowTab.HorizontalScrollbarSize = 10;
            this.aShowTab.Location = new System.Drawing.Point(4, 41);
            this.aShowTab.Name = "aShowTab";
            this.aShowTab.Size = new System.Drawing.Size(1078, 571);
            this.aShowTab.TabIndex = 0;
            this.aShowTab.Text = "SHOW";
            this.aShowTab.VerticalScrollbarBarColor = true;
            this.aShowTab.VerticalScrollbarHighlightOnWheel = false;
            this.aShowTab.VerticalScrollbarSize = 10;
            // 
            // showGrid
            // 
            this.showGrid.AllowUserToResizeRows = false;
            this.showGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.showGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.showGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.showGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.showGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.showGrid.ColumnHeadersHeight = 20;
            this.showGrid.Cursor = System.Windows.Forms.Cursors.IBeam;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.showGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.showGrid.EnableHeadersVisualStyles = false;
            this.showGrid.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.showGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.showGrid.Location = new System.Drawing.Point(3, 0);
            this.showGrid.Name = "showGrid";
            this.showGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.showGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.showGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            this.showGrid.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.showGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.showGrid.Size = new System.Drawing.Size(1072, 568);
            this.showGrid.TabIndex = 2;
            this.showGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.metroGrid1_CellContentClick);
            // 
            // aSearchTab
            // 
            this.aSearchTab.Controls.Add(this.searchButton);
            this.aSearchTab.Controls.Add(this.searchGrid);
            this.aSearchTab.Controls.Add(this.searchCombo);
            this.aSearchTab.Controls.Add(this.searchPrameter);
            this.aSearchTab.HorizontalScrollbarBarColor = true;
            this.aSearchTab.HorizontalScrollbarHighlightOnWheel = false;
            this.aSearchTab.HorizontalScrollbarSize = 10;
            this.aSearchTab.Location = new System.Drawing.Point(4, 41);
            this.aSearchTab.Name = "aSearchTab";
            this.aSearchTab.Size = new System.Drawing.Size(1078, 571);
            this.aSearchTab.TabIndex = 1;
            this.aSearchTab.Text = "SEARCH";
            this.aSearchTab.VerticalScrollbarBarColor = true;
            this.aSearchTab.VerticalScrollbarHighlightOnWheel = false;
            this.aSearchTab.VerticalScrollbarSize = 10;
            // 
            // searchButton
            // 
            this.searchButton.Location = new System.Drawing.Point(919, 12);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(97, 29);
            this.searchButton.TabIndex = 6;
            this.searchButton.Text = "Search";
            this.searchButton.UseSelectable = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // searchGrid
            // 
            this.searchGrid.AllowUserToResizeRows = false;
            this.searchGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.searchGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.searchGrid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.searchGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.searchGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.searchGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.searchGrid.DefaultCellStyle = dataGridViewCellStyle6;
            this.searchGrid.EnableHeadersVisualStyles = false;
            this.searchGrid.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.searchGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.searchGrid.Location = new System.Drawing.Point(2, 47);
            this.searchGrid.Name = "searchGrid";
            this.searchGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.searchGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.searchGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.searchGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.searchGrid.Size = new System.Drawing.Size(1014, 521);
            this.searchGrid.TabIndex = 5;
            // 
            // searchCombo
            // 
            this.searchCombo.FormattingEnabled = true;
            this.searchCombo.ItemHeight = 23;
            this.searchCombo.Location = new System.Drawing.Point(3, 12);
            this.searchCombo.Name = "searchCombo";
            this.searchCombo.Size = new System.Drawing.Size(304, 29);
            this.searchCombo.TabIndex = 4;
            this.searchCombo.UseSelectable = true;
            this.searchCombo.SelectedIndexChanged += new System.EventHandler(this.searchCombo_SelectedIndexChanged);
            // 
            // searchPrameter
            // 
            // 
            // 
            // 
            this.searchPrameter.CustomButton.Image = null;
            this.searchPrameter.CustomButton.Location = new System.Drawing.Point(541, 1);
            this.searchPrameter.CustomButton.Name = "";
            this.searchPrameter.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.searchPrameter.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.searchPrameter.CustomButton.TabIndex = 1;
            this.searchPrameter.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.searchPrameter.CustomButton.UseSelectable = true;
            this.searchPrameter.CustomButton.Visible = false;
            this.searchPrameter.Lines = new string[0];
            this.searchPrameter.Location = new System.Drawing.Point(325, 12);
            this.searchPrameter.MaxLength = 32767;
            this.searchPrameter.Name = "searchPrameter";
            this.searchPrameter.PasswordChar = '\0';
            this.searchPrameter.PromptText = "enter your text";
            this.searchPrameter.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.searchPrameter.SelectedText = "";
            this.searchPrameter.SelectionLength = 0;
            this.searchPrameter.SelectionStart = 0;
            this.searchPrameter.ShortcutsEnabled = true;
            this.searchPrameter.Size = new System.Drawing.Size(569, 29);
            this.searchPrameter.TabIndex = 3;
            this.searchPrameter.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.searchPrameter.UseCustomBackColor = true;
            this.searchPrameter.UseSelectable = true;
            this.searchPrameter.WaterMark = "enter your text";
            this.searchPrameter.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.searchPrameter.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            // 
            // aFilterTab
            // 
            this.aFilterTab.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.aFilterTab.Controls.Add(this.metroButton1);
            this.aFilterTab.Controls.Add(this.metroPanel1);
            this.aFilterTab.Controls.Add(this.employeeResult);
            this.aFilterTab.Controls.Add(this.searchEmployee);
            this.aFilterTab.Controls.Add(this.FilterCriteriaComboBox);
            this.aFilterTab.Controls.Add(this.FilterComboBox);
            this.aFilterTab.HorizontalScrollbarBarColor = true;
            this.aFilterTab.HorizontalScrollbarHighlightOnWheel = false;
            this.aFilterTab.HorizontalScrollbarSize = 10;
            this.aFilterTab.Location = new System.Drawing.Point(4, 41);
            this.aFilterTab.Name = "aFilterTab";
            this.aFilterTab.Size = new System.Drawing.Size(1078, 571);
            this.aFilterTab.TabIndex = 2;
            this.aFilterTab.Text = "FILTER";
            this.aFilterTab.VerticalScrollbarBarColor = true;
            this.aFilterTab.VerticalScrollbarHighlightOnWheel = false;
            this.aFilterTab.VerticalScrollbarSize = 10;
            this.aFilterTab.Click += new System.EventHandler(this.aFilterTab_Click);
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(797, 338);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(130, 23);
            this.metroButton1.TabIndex = 8;
            this.metroButton1.Text = "Generate Graph";
            this.metroButton1.UseSelectable = true;
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.btnFilter);
            this.metroPanel1.Controls.Add(this.rb4);
            this.metroPanel1.Controls.Add(this.rb3);
            this.metroPanel1.Controls.Add(this.rb2);
            this.metroPanel1.Controls.Add(this.rb1);
            this.metroPanel1.Controls.Add(this.lblsalaryRange);
            this.metroPanel1.Controls.Add(this.DateTo);
            this.metroPanel1.Controls.Add(this.lblDateTo);
            this.metroPanel1.Controls.Add(this.lblDateFrom);
            this.metroPanel1.Controls.Add(this.DateFrom);
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 8;
            this.metroPanel1.Location = new System.Drawing.Point(797, 42);
            this.metroPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(226, 264);
            this.metroPanel1.TabIndex = 7;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 8;
            // 
            // btnFilter
            // 
            this.btnFilter.Location = new System.Drawing.Point(0, 238);
            this.btnFilter.Margin = new System.Windows.Forms.Padding(2);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(89, 24);
            this.btnFilter.TabIndex = 11;
            this.btnFilter.Text = "Filter";
            this.btnFilter.UseSelectable = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // rb4
            // 
            this.rb4.AutoSize = true;
            this.rb4.Location = new System.Drawing.Point(4, 210);
            this.rb4.Margin = new System.Windows.Forms.Padding(2);
            this.rb4.Name = "rb4";
            this.rb4.Size = new System.Drawing.Size(97, 15);
            this.rb4.TabIndex = 10;
            this.rb4.Text = "above 100,000";
            this.rb4.UseSelectable = true;
            // 
            // rb3
            // 
            this.rb3.AutoSize = true;
            this.rb3.Location = new System.Drawing.Point(3, 190);
            this.rb3.Margin = new System.Windows.Forms.Padding(2);
            this.rb3.Name = "rb3";
            this.rb3.Size = new System.Drawing.Size(100, 15);
            this.rb3.TabIndex = 9;
            this.rb3.Text = "50,000-100,000";
            this.rb3.UseSelectable = true;
            // 
            // rb2
            // 
            this.rb2.AutoSize = true;
            this.rb2.Location = new System.Drawing.Point(3, 171);
            this.rb2.Margin = new System.Windows.Forms.Padding(2);
            this.rb2.Name = "rb2";
            this.rb2.Size = new System.Drawing.Size(94, 15);
            this.rb2.TabIndex = 8;
            this.rb2.Text = "10,000-50,000";
            this.rb2.UseSelectable = true;
            // 
            // rb1
            // 
            this.rb1.AutoSize = true;
            this.rb1.Location = new System.Drawing.Point(3, 149);
            this.rb1.Margin = new System.Windows.Forms.Padding(2);
            this.rb1.Name = "rb1";
            this.rb1.Size = new System.Drawing.Size(67, 15);
            this.rb1.TabIndex = 7;
            this.rb1.Text = "0-10,000";
            this.rb1.UseSelectable = true;
            // 
            // lblsalaryRange
            // 
            this.lblsalaryRange.AutoSize = true;
            this.lblsalaryRange.Location = new System.Drawing.Point(3, 119);
            this.lblsalaryRange.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblsalaryRange.Name = "lblsalaryRange";
            this.lblsalaryRange.Size = new System.Drawing.Size(86, 19);
            this.lblsalaryRange.TabIndex = 6;
            this.lblsalaryRange.Text = "Salary Range";
            // 
            // DateTo
            // 
            this.DateTo.Location = new System.Drawing.Point(3, 81);
            this.DateTo.Margin = new System.Windows.Forms.Padding(2);
            this.DateTo.MinimumSize = new System.Drawing.Size(4, 29);
            this.DateTo.Name = "DateTo";
            this.DateTo.Size = new System.Drawing.Size(151, 29);
            this.DateTo.TabIndex = 5;
            // 
            // lblDateTo
            // 
            this.lblDateTo.AutoSize = true;
            this.lblDateTo.Location = new System.Drawing.Point(3, 62);
            this.lblDateTo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDateTo.Name = "lblDateTo";
            this.lblDateTo.Size = new System.Drawing.Size(127, 19);
            this.lblDateTo.TabIndex = 4;
            this.lblDateTo.Text = "EmploymentDate To";
            // 
            // lblDateFrom
            // 
            this.lblDateFrom.AutoSize = true;
            this.lblDateFrom.Location = new System.Drawing.Point(3, 11);
            this.lblDateFrom.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDateFrom.Name = "lblDateFrom";
            this.lblDateFrom.Size = new System.Drawing.Size(146, 19);
            this.lblDateFrom.TabIndex = 3;
            this.lblDateFrom.Text = "EmploymentDate From";
            // 
            // DateFrom
            // 
            this.DateFrom.Location = new System.Drawing.Point(3, 32);
            this.DateFrom.Margin = new System.Windows.Forms.Padding(2);
            this.DateFrom.MinimumSize = new System.Drawing.Size(4, 29);
            this.DateFrom.Name = "DateFrom";
            this.DateFrom.Size = new System.Drawing.Size(151, 29);
            this.DateFrom.TabIndex = 2;
            // 
            // employeeResult
            // 
            this.employeeResult.AllowUserToResizeRows = false;
            this.employeeResult.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.employeeResult.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.employeeResult.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.employeeResult.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.employeeResult.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.employeeResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.employeeResult.DefaultCellStyle = dataGridViewCellStyle9;
            this.employeeResult.EnableHeadersVisualStyles = false;
            this.employeeResult.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.employeeResult.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.employeeResult.Location = new System.Drawing.Point(2, 104);
            this.employeeResult.Margin = new System.Windows.Forms.Padding(2);
            this.employeeResult.Name = "employeeResult";
            this.employeeResult.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.employeeResult.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.employeeResult.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.employeeResult.RowTemplate.Height = 24;
            this.employeeResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.employeeResult.Size = new System.Drawing.Size(791, 385);
            this.employeeResult.TabIndex = 6;
            // 
            // searchEmployee
            // 
            this.searchEmployee.Location = new System.Drawing.Point(488, 53);
            this.searchEmployee.Margin = new System.Windows.Forms.Padding(2);
            this.searchEmployee.Name = "searchEmployee";
            this.searchEmployee.Size = new System.Drawing.Size(56, 19);
            this.searchEmployee.TabIndex = 5;
            this.searchEmployee.Text = "Search";
            this.searchEmployee.UseSelectable = true;
            this.searchEmployee.Click += new System.EventHandler(this.searchEmployee_Click);
            // 
            // FilterCriteriaComboBox
            // 
            this.FilterCriteriaComboBox.FormattingEnabled = true;
            this.FilterCriteriaComboBox.ItemHeight = 23;
            this.FilterCriteriaComboBox.Location = new System.Drawing.Point(488, 12);
            this.FilterCriteriaComboBox.Name = "FilterCriteriaComboBox";
            this.FilterCriteriaComboBox.Size = new System.Drawing.Size(528, 29);
            this.FilterCriteriaComboBox.TabIndex = 3;
            this.FilterCriteriaComboBox.UseSelectable = true;
            // 
            // FilterComboBox
            // 
            this.FilterComboBox.FormattingEnabled = true;
            this.FilterComboBox.ItemHeight = 23;
            this.FilterComboBox.Location = new System.Drawing.Point(2, 12);
            this.FilterComboBox.Name = "FilterComboBox";
            this.FilterComboBox.Size = new System.Drawing.Size(480, 29);
            this.FilterComboBox.TabIndex = 2;
            this.FilterComboBox.UseSelectable = true;
            this.FilterComboBox.SelectedIndexChanged += new System.EventHandler(this.metroComboBox2_SelectedIndexChanged);
            // 
            // aGraphTab
            // 
            this.aGraphTab.BackColor = System.Drawing.SystemColors.Control;
            this.aGraphTab.Controls.Add(this.chart1);
            this.aGraphTab.HorizontalScrollbarBarColor = true;
            this.aGraphTab.HorizontalScrollbarHighlightOnWheel = false;
            this.aGraphTab.HorizontalScrollbarSize = 10;
            this.aGraphTab.Location = new System.Drawing.Point(4, 41);
            this.aGraphTab.Name = "aGraphTab";
            this.aGraphTab.Size = new System.Drawing.Size(1078, 571);
            this.aGraphTab.TabIndex = 3;
            this.aGraphTab.Text = "GRAPH";
            this.aGraphTab.VerticalScrollbarBarColor = true;
            this.aGraphTab.VerticalScrollbarHighlightOnWheel = false;
            this.aGraphTab.VerticalScrollbarSize = 10;
            // 
            // chart1
            // 
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(0, 19);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(1018, 551);
            this.chart1.TabIndex = 2;
            this.chart1.Text = "chart1";
            // 
            // aFacultyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1085, 675);
            this.Controls.Add(this.metroTabControl1);
            this.Name = "aFacultyForm";
            this.Text = "Faculty Information";
            this.Load += new System.EventHandler(this.HomePage_Load);
            this.metroTabControl1.ResumeLayout(false);
            this.aShowTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.showGrid)).EndInit();
            this.aSearchTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.searchGrid)).EndInit();
            this.aFilterTab.ResumeLayout(false);
            this.metroPanel1.ResumeLayout(false);
            this.metroPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.employeeResult)).EndInit();
            this.aGraphTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl metroTabControl1;
        private MetroFramework.Controls.MetroTabPage aShowTab;
        private MetroFramework.Controls.MetroTabPage aSearchTab;
        private MetroFramework.Controls.MetroTabPage aFilterTab;
        private MetroFramework.Controls.MetroTabPage aGraphTab;
        private MetroFramework.Controls.MetroGrid showGrid;
        private MetroFramework.Controls.MetroTextBox searchPrameter;
        private MetroFramework.Controls.MetroGrid searchGrid;
        private MetroFramework.Controls.MetroComboBox searchCombo;
        private MetroFramework.Controls.MetroComboBox FilterCriteriaComboBox;
        private MetroFramework.Controls.MetroComboBox FilterComboBox;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private MetroFramework.Controls.MetroButton searchEmployee;
        private MetroFramework.Controls.MetroGrid employeeResult;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroLabel lblsalaryRange;
        private MetroFramework.Controls.MetroDateTime DateTo;
        private MetroFramework.Controls.MetroLabel lblDateTo;
        private MetroFramework.Controls.MetroLabel lblDateFrom;
        private MetroFramework.Controls.MetroDateTime DateFrom;
        private MetroFramework.Controls.MetroRadioButton rb2;
        private MetroFramework.Controls.MetroRadioButton rb1;
        private MetroFramework.Controls.MetroRadioButton rb4;
        private MetroFramework.Controls.MetroRadioButton rb3;
        private MetroFramework.Controls.MetroButton btnFilter;
        //private software_projectDataSetTableAdapters.EMPLOYEEDETAILSTableAdapter eMPLOYEEDETAILSTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn mASKEDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dEPARTMENTDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dEPARTMENT1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn jOBTITLEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tENUREDATEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn eMPLOYMENTSTARTDATEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn aNNUALSALARYDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn yRTODATESALARYDataGridViewTextBoxColumn;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroButton searchButton;
    }
}

