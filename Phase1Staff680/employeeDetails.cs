﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Phase1Staff680
{
  public  class employeeDetails
    {
        [Key]
        public string masked { get; set; }
        public string department { get; set; }
        public string department1 { get; set; }
        public string job_title { get; set; }
        public DateTime? tenuredate { get; set; }
        public DateTime employment_startdate { get; set; }
        public decimal annual_salary { get; set; }
        public decimal yr_to_date_salary { get; set; }
    }
}
